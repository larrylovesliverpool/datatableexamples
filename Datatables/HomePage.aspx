﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="Datatables.HomePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        
        <div class="row">
                <h1>Employees Datatable</h1>
        </div>

        <div class="row">

            <div class="col-md-1">

            </div>
            <div class="col-md-10">
                <!-- datatable markup -->
                <table id="EmployeesDataTable"  class="display">
                    <thead>
                        <tr>
                            <th>Employee Number</th>
                            <th>Surname</th>
                            <th>Forename</th>
                            <th>Date of Birth</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="tr_EmployeesDataTable">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1">

            </div>

        </div>
    </div>


</asp:Content>
