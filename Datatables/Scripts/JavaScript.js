﻿

/* ******************* */
/* javascript / JQuery */
/* ******************* */

$(function () {

    /* initialise datatable */

    //getRamdomEmployee();

    var employeesDataTable = $('#EmployeesDataTable').DataTable({

        "serverSide": true,
        "ajax": {
            "url": "/api/Employee/",
            "type": 'POST'
        },
        "searching": true,
        "columns":
            [
                {
                    "data": "EmployeeNumber",
                    "className": "editPolicy",
                    "width": "15%"
                },
                {
                    "data": "Surname",
                    "className": "editPolicy",
                    "width": "30%"
                },
                {
                    "data": "Forename",
                    "className": "editPolicy",
                    "width": "30%"
                },
                {
                     "data": "DOB",
                     "className": "editPolicy",
                     "width": "20%"
                },
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "width":"5%"
                }
            ],
        "createdRow": function (row, data, index) {
            $('td', row).eq(0).parent('tr').addClass('AddedHere');
                    }
    });

    // ***********************************
    // event listener for Highlighting row
    // ***********************************

    $('#EmployeesDataTable tbody').on('mouseover', 'tr', function () {

        if ($(this).hasClass('AddedHere')) {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                employeesDataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        }

    });

    // ***********************************
    // event listener for Highlighting row
    // ***********************************

    $('#EmployeesDataTable tbody').on('mouseout', 'tr', function () {

        if ($(this).hasClass('AddedHere')) {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
        }

    });

    // **********************************************
    // event listener for opening and closing details
    // **********************************************

    $('#EmployeesDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = employeesDataTable.row(tr);

        $(this).addClass('selected');      

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(AddressHTMLMarkUp()).show();
            tr.addClass('shown');
            tr.removeClass('selected');
            AddressGetRecord(row.data());
        }
    });


});

function AddressHTMLMarkUp() {

    // ****************
    // Load html markup
    // ****************

    var htmlMarkUp = "";

    jQuery.ajaxSetup({ async: false });

    $.get('/Forms/Employee/MarkUp/EmployeeAddress.html', function (data) {
        htmlMarkUp = data;
    });

    jQuery.ajaxSetup({ async: true });

    return htmlMarkUp;
}

function AddressGetRecord(rowData) {
    var addressData = GetCompany( rowData.AddressId );
}

function GetCompany(Id) {

    var url = "/api/address/" + Id;
    var addressData;

    jQuery.ajaxSetup({ async: false });

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'AddressById',
        data: { Id: Id },
        async: false,
        success: function (data) {
            addressData = eval('(' + data + ')');
        },
        error: function (jqxhr, textStatus, error) {
            alert(textStatus + ' : ' + jqxhr);
        },
    });

    jQuery.ajaxSetup({ async: true });

    return addressData;
}

function DisplayAddress(addressRec) {

}

function getRamdomEmployee() {

    var user = {};

    jQuery.ajaxSetup({ async: false });

    $.ajax({
        url: 'https://randomuser.me/api/?results=100&nat=gb',
        dataType: 'json',
        success: function (data) {
            users = data;
        }
    });

    jQuery.ajaxSetup({ async: true });

    // array of 100 records
    var dataArray = users["results"];
    var index = 0;
    var surname;
    var forename;

    jQuery.ajaxSetup({ async: false });

    for (index = 0; index < 100; index++) {

        surname = dataArray[index].user.name.last;
        forename = dataArray[index].user.name.first;

        var url = "/api/employee/"
        var success = false;

        // convert into json string as details object
        var jsonobj = JSON.stringify({ forename: forename, surname: surname });

        $.ajax({
            url: url,
            dataType: 'json',
            contentType: "application/json",
            type: 'InsertEmployee',
            data: jsonobj,
            success: function (data) {
                success = data;
            },
            error: function (jqxhr, textStatus, error) {
                alert(textStatus + ' : ' + jqxhr);
            },
        });
    }

    jQuery.ajaxSetup({ async: true });

    return user;

}