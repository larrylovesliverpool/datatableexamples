﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Services;
using System.Web.Script.Serialization;

using Datatables.BLL;
using Datatables.Model;

namespace Datatables.Controllers
{
    public class AddressController : ApiController
    {

        [AcceptVerbs("AddressById")]
        public string AddressById(int Id)
        {
            BLL.Address addressLogic = new BLL.Address();
            Model.Address addressData = new Model.Address();

            addressData = addressLogic.Read(Id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string serializedResult = serializer.Serialize(addressData);

            return serializedResult;
        }
    }
}