﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Datatables.Model;
using Datatables.DAL;

namespace Datatables.Controllers
{
    public class EmployeeController : ApiController
    {
        // POST api/<controller>
        [HttpPost]
        public object Post(JObject pObj)
        {
            // *************************************
            // datatable options passed as an object
            // *************************************

            DatatablesParamters dtParams = new DatatablesParamters((int)pObj["draw"],
                                                                   (int)pObj["start"],
                                                                   (int)pObj["length"],
                                                                   (string)pObj["search"]["value"],
                                                                   (bool)pObj["search"]["regex"],
                                                                   (int)pObj["order"][0]["column"],
                                                                   (string)pObj["order"][0]["dir"]);

            BLL.Employees employessData = new BLL.Employees();

            // *********************************************************
            // object to return back to datatable containg results set.
            // only include error, if have error to report as datatables 
            // reports if included.
            // error = "Error string message here!"
            // *********************************************************

            return new
            {
                draw = dtParams.Draw,
                recordsTotal = employessData.ReadNumberOfEmployees(),
                recordsFiltered = employessData.ReadFiltered(dtParams.Search),
                data = employessData.ReadDataTable(dtParams),
            };
        }

        [AcceptVerbs("InsertEmployee")]
        public int InsertEmployee(JObject pObj)
        {
            BLL.Employees employessBLL = new BLL.Employees();
            Model.Employees employeesModel = new Model.Employees();

            employeesModel.Surname = (string)pObj["surname"];
            employeesModel.Forename = (string)pObj["forename"];

            return employessBLL.Insert(employeesModel);
        }
    }
}