﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;

namespace Datatables.Model
{
    public class DatatablesParamters
    {
        // *****************************************************************
        // When making a request to the server using server-side processing, 
        // DataTables will send the following data in order to let the 
        // server know what data is required:
        // https://datatables.net/manual/server-side
        // *****************************************************************

        private int _draw;
        private int _start;
        private int _length;
        private string _search;
        private bool _regex;

        // datatables allows for multiple column actions
        // model only allows for single column options using
        // index of 0

        private int _orderColumn;
        private string _orderDirection;

        public int Draw
        {
            get { return _draw; }
            set { _draw = value; }
        }
        public int Start
        {
            get { return _start; }
            set { _start = value; }
        }
        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }
        public string Search
        {
            get { return _search; }
            set { _search = value; }
        }
        public bool Regex
        {
            get { return _regex; }
            set { _regex = value; }
        }
        public int OrderColumn
        {
            get { return _orderColumn; }
            set { _orderColumn = value; }
        }
        public string OrderDirection
        {
            get { return _orderDirection; }
            set { _orderDirection = value; }
        }

        // ****

        public DatatablesParamters() { }

        public DatatablesParamters( int draw,
                                    int start,
                                    int length,
                                    string search,
                                    bool regex,
                                    int orderColumn,
                                    string orderDirection) {

            this.Draw = draw;
            this.Start = start;
            this.Length = length;
            this.Search = search;
            this.Regex = regex;
            this.OrderColumn = orderColumn;
            this.OrderDirection = orderDirection;
        }

        public DatatablesParamters(JObject parameters) {

            // draw counter
            Draw = (int)parameters["draw"];
            //number of rows
            Length = (int)parameters["length"];
            //page
            Start = (int)parameters["start"];
            //get first column sorting
            OrderColumn = (int)parameters["order"][0]["column"];
            OrderDirection = (string)parameters["order"][0]["dir"];
            //search string
            Search = (string)parameters["search"]["value"];
        }
    }
}
