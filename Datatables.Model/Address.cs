﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datatables.Model
{
    public class Address
    {
        private int addressId;
        private int addressType;
        private string addressNumber;
        private string addressName;
        private string addressLine1;
        private string addressLine2;
        private string addressLine3;
        private string addressLine4;
        private string addressLine5;
        private string postcode;

        public int AddressId
        {
            get { return addressId; }
            set { addressId = value; }
        }
        public int AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }
        public string AddressNumber
        {
            get { return addressNumber; }
            set { addressNumber = value; }
        }
        public string AddressName
        {
            get { return addressName; }
            set { addressName = value; }
        }
        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }
        public string AddressLine3
        {
            get { return addressLine3; }
            set { addressLine3 = value; }
        }
        public string AddressLine4
        {
            get { return addressLine4; }
            set { addressLine4 = value; }
        }
        public string AddressLine5
        {
            get { return addressLine5; }
            set { addressLine5 = value; }
        }
        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }
        }
    }
}
