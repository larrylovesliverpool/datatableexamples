﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datatables.Model
{
    /* ************************* */
    /* Basic employee data model */
    /* ************************* */

    public class Employees
    {
        private int employeeNumber;
        private string surname;
        private string forename;
        private DateTime dob;
        private DateTime startDate;
        private DateTime endDate;
        private int addressId;

        public int EmployeeNumber
        {
            get { return employeeNumber; }
            set { employeeNumber = value; }
        }
        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }
        public string Forename
        {
            get { return forename; }
            set { forename = value; }
        }
        public DateTime DOB
        {
            get { return dob; }
            set { dob = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public int AddressId
        {
            get { return addressId; }
            set { addressId = value; }
        }
    }

    /* ************************************************************** */
    /* Basic employee data model with row num for datatable selection */
    /* ************************************************************** */

    public class EmployeesDatatable : Employees
    {
        private System.Int64 _rowNum;

        public System.Int64 RowNum
        {
            get { return _rowNum; }
            set { _rowNum = value; }
        }

    }

}
