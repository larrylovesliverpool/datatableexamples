﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Datatables.Model;

namespace Datatables.BLL
{
    public class Employees
    {
        public Employees() { }

        public Datatables.Model.Employees Read(int employeeId)
        {
            return new DAL.Employees().Read(employeeId);
        }

        public List<Datatables.Model.Employees> Read() {

            return new DAL.Employees().Read();
        }

        public List<Datatables.Model.EmployeesDatatable> ReadDataTable( int displayLength,
                                                                        int displayStart,
                                                                        int sortCol,
                                                                        string sortDir,
                                                                        string search ) {

            return new DAL.Employees().ReadPage(displayLength,
                                                displayStart,
                                                sortCol,
                                                sortDir,
                                                search);
        }

        public List<Datatables.Model.EmployeesDatatable> ReadDataTable( Model.DatatablesParamters dtp)
        {

            return new DAL.Employees().ReadPage(dtp.Length,
                                                dtp.Start,
                                                dtp.OrderColumn,
                                                dtp.OrderDirection,
                                                dtp.Search);
        }

        public int ReadNumberOfEmployees() {

            return new DAL.Employees().NumberOfEmployees();
        }

        public int ReadFiltered(string search) {

            return new DAL.Employees().Filtered(search);
        }

        public int Insert( Model.Employees employee ) {

            return new DAL.Employees().Insert(employee.Surname,employee.Forename);
        }
    }
}
