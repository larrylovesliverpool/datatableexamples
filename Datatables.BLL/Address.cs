﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Datatables.Model;

namespace Datatables.BLL
{
    public class Address
    {
        public Address() { }

        public Datatables.Model.Address Read(int AddressId)
        {
            return new DAL.Address().Read(AddressId);
        }
    }
}
