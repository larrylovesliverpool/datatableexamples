USE [WebApp]
GO
/****** Object:  StoredProcedure [dbo].[EmployeesDatatable_Select]    Script Date: 24/07/2018 14:07:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[EmployeesDatatable_Select]
	@DisplayLength int,
	@DisplayStart int,
	@SortCol int,
	@SortDir nvarchar(10),
	@Search nvarchar(255) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @FirstRec int, @LastRec int
	SET @FirstRec = @DisplayStart;
	SET @LastRec = @DisplayStart + @DisplayLength;

	WITH CTE_Employee AS
	(
		SELECT ROW_NUMBER() OVER (ORDER BY 
			CASE WHEN (@SortCol = 0 AND @SortDir = 'asc' )
			 then [EmployeeNumber]
			END asc,
			CASE WHEN (@SortCol = 0 AND @SortDir = 'desc' )
			 then [EmployeeNumber]
			END desc,
			CASE WHEN (@SortCol = 1 AND @SortDir = 'asc' )
			 then [Surname]
			END asc,
			CASE WHEN (@SortCol = 1 AND @SortDir = 'desc' )
			 then [Surname]
			END desc
		)
		AS	RowNum,
			[EmployeeNumber],
			[Surname],
			[Forename],
			[DOB],
			[StartDate],
			[EndDate],
			[AddressID]
		From Employees
		WHERE ( @Search IS Null
				OR [EmployeeNumber] like '%' + @search + '%'
				OR [Surname] like '%' + @search + '%'
				OR [Forename] like '%' + @search + '%'
				) 
		)
		SELECT *
		FROM CTE_Employee
		WHERE RowNum > @FirstRec AND RowNum <= @LastRec

	RETURN @@ROWCOUNT
END
