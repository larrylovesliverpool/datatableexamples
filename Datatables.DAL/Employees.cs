﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

using System.Data;
using System.Data.SqlClient;

using Datatables.Model;

namespace Datatables.DAL
{
    public class Employees : DataAccess
    {

        /* ********************************************* */
        /* total number of employee records in datatbase */
        /* ********************************************* */

        public int NumberOfEmployees()
        {
            SqlCommand command = new SqlCommand();
            command = BuildSqlCommand("EmployeesNumberOf_Select");

            try
            {
                return (int)ExecuteScalar(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Insert( string surname, string forename )
        {

            SqlCommand command = new SqlCommand();
            command = BuildSqlCommand("Employee_Insert");

            command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            command.Parameters.Add("@surname", SqlDbType.NVarChar).Value = surname;
            command.Parameters.Add("@forename", SqlDbType.NVarChar).Value = forename;

            try
            {
                command.Connection.Open();
                ExecuteNonQuery(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
            }

            return (int)command.Parameters["@RETURN_VALUE"].Value;
        }

        /* ******************************************** */
        /* total number of employees filtered by search */
        /* ******************************************** */

        public int Filtered(string search)
        {
            SqlCommand command = new SqlCommand();
            command = BuildSqlCommand("EmployeesFiltered_Select");

            command.Parameters.Add("@search", SqlDbType.NVarChar).Value = search;

            try
            {
                return (int)ExecuteScalar(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Model.Employees Read(int employeeId) {
            return new Model.Employees();
        }

        public List<Model.Employees> Read() {

            List<Model.Employees> employees = new List<Model.Employees>();

            SqlCommand command = new SqlCommand();
            DataTable resultSet = new DataTable();
            command = BuildSqlCommand("Employees_Select");

            try
            {
                resultSet = DataTableResults(command);

                // for each row returned from db.

                foreach (DataRow row in resultSet.Rows) {

                    Model.Employees employee = new Model.Employees();
                    System.Reflection.PropertyInfo[] propInfo = employee.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo pInfo in propInfo)
                    {
                        if (row.Table.Columns.Contains(pInfo.Name.ToString()))
                        {
                            pInfo.SetValue(employee, row[pInfo.Name.ToString()]);
                        }
                    }

                    employees.Add(employee);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
            return employees;
        }

        /* ********************************************************** */
        /* employees list filtered by search, pagelength, page number */
        /* order by column and as or desc on column                   */
        /* ********************************************************** */

        public List<Model.EmployeesDatatable> ReadPage( int displayLength,
                                                        int displayStart,
                                                        int sortCol,
                                                        string sortDir,
                                                        string search ) {

            List<Model.EmployeesDatatable> employees = new List<Model.EmployeesDatatable>();

            SqlCommand command = new SqlCommand();
            DataTable resultSet = new DataTable();
            command = BuildSqlCommand("EmployeesDatatable_Select");

            command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            command.Parameters.Add("@displayLength", SqlDbType.Int).Value = displayLength;
            command.Parameters.Add("@displayStart", SqlDbType.Int).Value = displayStart;
            command.Parameters.Add("@sortCol", SqlDbType.Int).Value = sortCol;
            command.Parameters.Add("@sortDir", SqlDbType.NVarChar).Value = sortDir;
            command.Parameters.Add("@search", SqlDbType.NVarChar).Value = search;

            try
            {
                resultSet = DataTableResults(command);

                foreach (DataRow row in resultSet.Rows)
                {

                    Model.EmployeesDatatable employee = new Model.EmployeesDatatable();
                    System.Reflection.PropertyInfo[] propInfo = employee.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo pInfo in propInfo)
                    {
                        if (row.Table.Columns.Contains(pInfo.Name.ToString()))
                        {
                            if (row[pInfo.Name.ToString()] != DBNull.Value )
                            {
                                pInfo.SetValue(employee, row[pInfo.Name.ToString()]);
                            }                            
                        }
                    }

                    employees.Add(employee);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return employees;
        }
    }
}
