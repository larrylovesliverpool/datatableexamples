﻿using System;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;

namespace Datatables.DAL
{
    public class Connection
    {
        private string _DBConnectionString;

        public string DBConnectionString
        {
            get
            {
                try
                {
                    _DBConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                }
                catch
                {
                    _DBConnectionString = string.Empty;
                }

                return _DBConnectionString;
            }
            set
            {
                _DBConnectionString = value;
            }
        }

        public Connection() { }

        public Connection(string connectionName) { }

        // ***********************
        // make default connection
        // ***********************
        public SqlConnection makeConnection()
        {
            return new SqlConnection(DBConnectionString);
        }

        // *********************
        // make named connection
        // *********************
        public SqlConnection makeConnection(string connectionName)
        {

            DBConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            return new SqlConnection(DBConnectionString);
        }

    }
}
