﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.Data;
using System.Data.SqlClient;

using Datatables.Model;

namespace Datatables.DAL
{
    public class Address : DataAccess
    {
        public Model.Address Read(int addressId)
        {
            Model.Address addressRec = new Model.Address();

            SqlCommand command = new SqlCommand();
            //DataTable resultSet = new DataTable();
            DataRow resultSet;

            command = BuildSqlCommand("EmployeeAddress_Select");

            command.Parameters.Add("@addressId", SqlDbType.Int).Value = addressId;

            try
            {
                resultSet = DataRowResults(command);

                System.Reflection.PropertyInfo[] propInfo = addressRec.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo pInfo in propInfo)
                {
                    if (resultSet.Table.Columns.Contains(pInfo.Name.ToString()))
                    {
                        if (resultSet[pInfo.Name.ToString()] != DBNull.Value)
                        {
                            pInfo.SetValue(addressRec, resultSet[pInfo.Name.ToString()]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return addressRec;
        }
    }
}
