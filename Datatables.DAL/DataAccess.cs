﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace Datatables.DAL
{
    public class DataAccess : Connection
    {
        public DataAccess() { }

        public DataAccess(string connectionString) : base(connectionString) { }

        public DataTable DataTableResults(SqlCommand command)
        {
            DataTable resultSet = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

            command.Connection.Open();
            dataAdapter.Fill(resultSet);
            command.Connection.Close();

            return resultSet;
        }

        public DataRow DataRowResults(SqlCommand command)
        {
            DataTable resultSet = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            SqlConnection connection = makeConnection();

            connection.Open();
            dataAdapter.Fill(resultSet);
            connection.Close();

            if (resultSet.Rows.Count == 1)
            {
                return resultSet.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public void ExecuteNonQuery(SqlCommand command)
        {
            SqlConnection connection = makeConnection();

            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
        }

        //public int ExecuteScalar(SqlCommand command)
        //{

        //    int value = 0;

        //    SqlConnection connection = makeConnection();

        //    try
        //    {
        //        connection.Open();
        //        value = (int)command.ExecuteScalar();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        connection.Close();
        //    }

        //    return value;
        //}

        public object ExecuteScalar(SqlCommand command)
        {
            object value;

            try
            {
                command.Connection.Open();
                value = (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                command.Connection.Close();
            }

            return value;
        }

        public SqlCommand BuildSqlCommand()
        {
            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.Connection = makeConnection();
            sqlCommand.CommandType = CommandType.StoredProcedure;

            return sqlCommand;
        }

        public SqlCommand BuildSqlCommand(string name)
        {
            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.Connection = makeConnection();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = name;

            return sqlCommand;
        }

    }
}
